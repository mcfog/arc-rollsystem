package star191.lab.architecture.rollers.interfaces;

public interface IRollerReader {
    public String read();
}
