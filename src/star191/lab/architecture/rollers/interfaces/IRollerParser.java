package star191.lab.architecture.rollers.interfaces;

public interface IRollerParser {
    public String parse(String text);
}
