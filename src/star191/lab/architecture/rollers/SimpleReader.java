package star191.lab.architecture.rollers;

import star191.lab.architecture.rollers.interfaces.IRollerReader;

public class SimpleReader implements IRollerReader {

    private String data;

    public SimpleReader(String data){
        this.data = data;
    }

    @Override
    public String read() {
        return data;
    }
}
