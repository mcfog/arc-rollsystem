package star191.lab.architecture.rollers;

import star191.lab.architecture.rollers.abstracts.AbstractRoller;
import star191.lab.architecture.rollers.interfaces.IRollerParser;

import java.util.ArrayList;
import java.util.List;

public class KMRPRoller extends AbstractRoller {
    private final ArrayList<Double> results;
    public static final IRollerParser KMRPParser = new KMRPParser();

    private static class KMRPParser implements IRollerParser{

        @Override
        public String parse(String text) {
            return text;
        }
    }

    public KMRPRoller(IRollerParser parser) {
        super(parser);

        results = new ArrayList<>();
    }

    @Override
    public List<Double> rolling(String parsed) {

        /*
        parsedString -> ??? -> List
         */

        return results;
    }


}
