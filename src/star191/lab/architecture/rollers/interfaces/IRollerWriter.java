package star191.lab.architecture.rollers.interfaces;

import java.util.List;

public interface IRollerWriter {
    public void write(List data);
}
