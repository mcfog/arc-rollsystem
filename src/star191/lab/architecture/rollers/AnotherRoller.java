package star191.lab.architecture.rollers;

import star191.lab.architecture.rollers.abstracts.AbstractRoller;
import star191.lab.architecture.rollers.interfaces.IRollerParser;

import java.util.ArrayList;
import java.util.List;

public class AnotherRoller extends AbstractRoller {
    private final ArrayList<Integer> results;
    public static final IRollerParser SOMEPARSER = new SomeParser();

    private static class SomeParser implements IRollerParser{

        @Override
        public String parse(String text) {
            String parsed = "meow " + text + " meow";

            return parsed;
        }
    }

    public AnotherRoller(IRollerParser parser) {
        super(parser);

        results = new ArrayList<>();
    }


    @Override
    public List<Integer> rolling(String parsed) {

        /*
        parsedString -> ??? -> List
         */

        return results;
    }


}
