package star191.lab.architecture.rollers.abstracts;
import star191.lab.architecture.rollers.interfaces.IRollerParser;

import java.util.List;

public abstract class AbstractRoller<T>  {

    private final IRollerParser parser;

    public AbstractRoller(IRollerParser parser){
        this.parser = parser;
    }

    public abstract List<T> rolling(String parsed);

    public List<T> roll(String text) {

        String parsedString = parser.parse(text);

        return rolling(parsedString);
    }
}
