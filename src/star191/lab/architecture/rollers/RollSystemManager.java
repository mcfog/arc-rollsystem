package star191.lab.architecture.rollers;

import star191.lab.architecture.rollers.abstracts.AbstractRoller;
import star191.lab.architecture.rollers.interfaces.IRollerReader;
import star191.lab.architecture.rollers.interfaces.IRollerWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RollSystemManager {

    private final ArrayList<AbstractRoller> rollers;

    public RollSystemManager(AbstractRoller... rollers){
        this.rollers = new ArrayList<>(Arrays.asList(rollers));
    }

    public void roll(IRollerReader reader, IRollerWriter writer) {

        // Read data from reader
        String text = reader.read();

        // Choice the roll system
        AbstractRoller choosenSystem = systemChoice(text);

        // Roll
        List results = choosenSystem.roll(text);

        // Write data to writer
        writer.write(results);
    }

    public void addRoller(AbstractRoller roller) {
        rollers.add(roller);
    }

    private AbstractRoller systemChoice(String text) {
        int index;

        /*
        just some parsing magic
         */

        index = 0;

        return rollers.get(index);
    }
}
