package star191.lab.architecture.rollers;

import star191.lab.architecture.rollers.interfaces.IRollerReader;
import star191.lab.architecture.rollers.interfaces.IRollerWriter;

public class Main {

    public static void main(String[] args) {

        // Create rollers
        KMRPRoller roller1 = new KMRPRoller(KMRPRoller.KMRPParser);
        AnotherRoller roller2 = new AnotherRoller(AnotherRoller.SOMEPARSER);
        AnotherRoller roller3 = new AnotherRoller(AnotherRoller.SOMEPARSER);


        // Create RollSystem Manager
        final RollSystemManager rollSystemManager = new RollSystemManager(roller1, roller2);
        rollSystemManager.addRoller(roller3);


        // Create reader and writer
        IRollerReader reader1 = new SimpleReader("3d6");
        IRollerWriter writer1 = new SimpleWriter();
        
        // Roll! Read from reader1 and write to writer1
        rollSystemManager.roll(reader1, writer1);


        // Another reader, reader2!
        IRollerReader reader2 = new SimpleReader("%9");
        
        // Roll! Read from reader2 and write to writer1
        rollSystemManager.roll(reader2, writer1);


        // Great work!
    }
}
